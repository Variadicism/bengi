module Beng.Build.Ops where

import Beng.Types.Parse.Parsers.Code (Parser)
import Beng.Types.Ops (Op)

build :: String -> Parser Op