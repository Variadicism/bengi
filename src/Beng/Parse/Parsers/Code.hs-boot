module Beng.Parse.Parsers.Code where

import Utils.Messaging (Messagable)

import Beng.Types.Parse.Parsers.Code (State)

instance Show State

instance Messagable State